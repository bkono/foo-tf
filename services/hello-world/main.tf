module "repos" {
  source = "../../modules/docker-service-source"

  name        = "hello-world"
  description = "A happy greeter."
}

module "ecr-build-pipeline" {
  source    = "../../modules/pipelines/codecommit-ecr-build-only"
  namespace = "kono"
  name      = "hello-world"
  stage     = "dev"
}
