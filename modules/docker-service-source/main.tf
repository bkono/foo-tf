variable "name" {
  type        = "string"
  description = "Name of the service to use when creating the CodeCommit and ECR repositories."
}

variable "description" {
  type        = "string"
  description = "Brief service description to include with the repository."
}

resource "aws_codecommit_repository" "default" {
  repository_name = "${var.name}"
  description     = "${var.description}"
}

resource "aws_ecr_repository" "default" {
  name = "${var.name}"
}

output "codecommit_repo_name" {
  value = "${aws_codecommit_repository.default.repository_name}"
}

output "codecommit_repo_clone_url_ssh" {
  value = "${aws_codecommit_repository.default.clone_url_ssh}"
}

output "ecr_repo_url" {
  value = "${aws_ecr_repository.default.repository_url}"
}

