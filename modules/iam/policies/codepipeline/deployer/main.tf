variable "name" {
  type        = "string"
  description = "Name for the policy document"
}

variable "resources" {
  type        = "list"
  default     = ["*"]
  description = "Resources to apply the codecommit policy to."
}

variable "role" {
  type        = "string"
  description = "ARN of the role to attach the policy to."
}

resource "aws_iam_role_policy_attachment" "default" {
  role       = "${var.role}"
  policy_arn = "${aws_iam_policy.default.arn}"
}

resource "aws_iam_policy" "default" {
  name   = "${var.name}-codepipeline"
  policy = "${data.aws_iam_policy_document.default.json}"
}

data "aws_iam_policy_document" "default" {
  statement {
    sid = ""

    actions = [
      "elasticbeanstalk:*",
      "ec2:*",
      "elasticloadbalancing:*",
      "autoscaling:*",
      "cloudwatch:*",
      "s3:*",
      "sns:*",
      "cloudformation:*",
      "rds:*",
      "sqs:*",
      "ecr:*",
      "ecs:*",
      "iam:PassRole",
    ]

    resources = "${var.resources}"
    effect    = "Allow"
  }
}

module "codecommit" {
  source = "../../codecommit/deployer"
  name   = "${var.name}"
  role   = "${var.role}"
}
