variable "name" {
  type        = "string"
  description = "Name for the policy document"
}

variable "resources" {
  type        = "list"
  default     = ["*"]
  description = "Resources to apply the codecommit policy to."
}

variable "role" {
  type        = "string"
  description = "ARN of the role to attach the policy to."
}

resource "aws_iam_role_policy_attachment" "default" {
  role       = "${var.role}"
  policy_arn = "${aws_iam_policy.default.arn}"
}

resource "aws_iam_policy" "default" {
  name   = "${var.name}-codecommit"
  policy = "${data.aws_iam_policy_document.default.json}"
}

data "aws_iam_policy_document" "default" {
  statement {
    sid = ""

    actions = [
      "codecommit:GetBranch",
      "codecommit:GetCommit",
      "codecommit:UploadArchive",
      "codecommit:GetUploadArchiveStatus",
      "codecommit:CancelUploadArchive",
    ]

    resources = "${var.resources}"
    effect    = "Allow"
  }
}
